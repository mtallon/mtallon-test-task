#!/usr/bin/python
import sys
import re
import bs4

def extractHTML(htmlFile):
    with open(htmlFile, 'r') as f:
        webpage = f.read().decode('utf-8')
    return bs4.BeautifulSoup(webpage, "html.parser")

def checkMatches(origin, sample):
    """
        There are 3 different scenarios:
        Match class and href (btn btn-success and #ok)
        Match title and class (Make-button and btn btn-success)
        Match title and href (Make-button and #ok)
    """

    nodeTags = {
        "class": ' '.join(origin['class']),
        "href" : origin['href'],
        "title": origin['title'],
        "text" : origin.getText().strip()
    }

    return sample.find(attrs= {'class': nodeTags['class'], 'href': nodeTags['href']}) or \
           sample.find(title = nodeTags['title'], attrs= {'class': nodeTags['class']}) or \
           sample.find(title = nodeTags['title'], attrs= {'href': nodeTags['href']})

def xpath_soup(element):
    #BS4 xpath generator, https://gist.github.com/ergoithz/6cf043e3fdedd1b94fcf
    components = []
    child = element if element.name else element.parent
    for parent in child.parents:
        siblings = parent.find_all(child.name, recursive=False)
        components.append(
            child.name
            if siblings == [child] else
            '%s[%d]' % (child.name, 1 + siblings.index(child))
            )
        child = parent
    components.reverse()
    return '/%s' % '/'.join(components)

def main():
    if len (sys.argv) != 3 :
        print "Usage: python crawler.py <input_origin_file_path> <input_other_sample_file_path>"
        sys.exit(1)

    #we assume that the original file contains the id='make-everything-ok-button'
    origin = extractHTML(sys.argv[1]).find(id='make-everything-ok-button')
    sample = extractHTML(sys.argv[2])

    elem = checkMatches(origin, sample)
    del origin, sample

    if elem:
        print "Element found!\n%sFull xpath element:\n%s" % (elem.prettify(), xpath_soup(elem))
    else:
        print "New rules is needed"

if __name__ == "__main__":
    main()
