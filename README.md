# HTML Crawler

Analyzes HTML and finds a specific element, even after changes, using a set of extracted attributes.


### Install BS4

```sh
pip install -r requirements.txt
or
pip install bs4
```

Usage example (demo files):
```sh
    python crawler.py startbootstrap-sb-admin-2-examples/sample-0-origin.html startbootstrap-sb-admin-2-examples/sample-1-evil-gemini.html
    python crawler.py startbootstrap-sb-admin-2-examples/sample-0-origin.html startbootstrap-sb-admin-2-examples/sample-2-container-and-clone.html
    python crawler.py startbootstrap-sb-admin-2-examples/sample-0-origin.html startbootstrap-sb-admin-2-examples/sample-3-the-escape.html
    python crawler.py startbootstrap-sb-admin-2-examples/sample-0-origin.html startbootstrap-sb-admin-2-examples/sample-4-the-mash.html
```

Ouput:
```sh
    python crawler.py startbootstrap-sb-admin-2-examples/sample-0-origin.html startbootstrap-sb-admin-2-examples/sample-1-evil-gemini.html
    
    Element found!
    <a class="btn btn-success" href="#check-and-ok" onclick="javascript:window.okDone(); return false;" rel="done" title="Make-Button">
     Make everything OK
    </a>
    Full xpath element:
    /html/body/div/div/div[3]/div[1]/div/div[2]/a[2]
    
```